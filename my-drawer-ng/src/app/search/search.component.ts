import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        console.log("inicializacion");
        console.log({nombre : {nombre : {nombre : {nombre : "santi"}}}});
        console.dir({nombre : {nombre : {nombre : {nombre : "santi"}}}});
        console.log([1,2,3]);
        console.dir([1,2,3]);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
