import { Directive, Input } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
    selector : "[minLong]",
    providers : [{
        provide : NG_VALIDATORS,
        useExisting : MinLongDirective,
        multi : true
    }]
})
export class MinLongDirective implements Validator
{
    @Input() minLong : string;

    constructor() {}

    validate(control : AbstractControl) : {[key : string] : any} {
        return !control.value || control.value.length >= (this.minLong || 2) ? null : {minLong : true};
    }
}