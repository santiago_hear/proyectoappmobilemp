import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, borderTopLeftRadiusProperty, Color, View } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { FavoritosService } from "../domain/favoritos.service";
import { toBase64String } from "@angular/compiler/src/output/source_map";
import * as Toast from "nativescript-toasts";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import * as SocialShare from "nativescript-social-share";
import { GestureTypes } from "@nativescript/core/ui/gestures";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
    // providers : [NoticiasService]
})
export class SearchComponent implements OnInit {

    resultados : Array<string>;
    @ViewChild("layout") layout : ElementRef;

    constructor(
        public noticias : NoticiasService, 
        public favoritos : FavoritosService, 
        public store : Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({text : "Nuestras sugerencias: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    addFavorites(s : string)
    {
        this.favoritos.agregarFavorito(s);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args) : void
    {
        console.log("entro al tap");
        this.store.dispatch( new NuevaNoticiaAction( new Noticia (args.view.bindingContext.texto)));
    }

    onLongPress(s) : void
    {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!")
    }

    searchNow(s : string)
    {
        // this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor : new Color("green"),
        //     duration : 300,
        //     delay : 150
        // }).then(() => layout.animate({
        //     backgroundColor : new Color("white"),
        //     duration : 300,
        //     delay : 150
        // }));

        console.dir("Buscando..." + s);
        this.noticias.buscar(s).then((r : any) => {
            console.log("resultados de la busqueda: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error en la busqueda: " + e);
            Toast.show({text : "Error en la busqueda", duration : Toast.DURATION.SHORT})
        })
    }
}
