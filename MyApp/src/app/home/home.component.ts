import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ImageAsset, isAndroid, isIOS } from "@nativescript/core";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { Favorito } from "../domain/favoritos-state.model";
//import { Image } from "@nativescript/core/ui/image";
import * as Camara from "@nativescript/camera";
import * as SocialShare from "nativescript-social-share";
import { Subject } from "rxjs";
import { Image } from "@nativescript/core/ui/image";


@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    mensajeBienvenida : string = "";

    favoritos : Favorito[];

    constructor(private store : Store<AppState>) { }

    ngOnInit(): void {
        if (isAndroid)
        {
            this.mensajeBienvenida = "Estamos en android";
        }
        if(isIOS)
        {
            this.mensajeBienvenida = "Estamos en IOS";
        }
        else{
            this.mensajeBienvenida = "Android";
        }
        this.store.select((state) => state.favoritos.items)
            .subscribe((data) => {
                console.dir(data);
                this.favoritos = data;
                console.log(this.favoritos[0].texto);
            });
    }

    onButtonTap() : void
    {
        Camara.requestPermissions().then(
            function success() {
                const options = {width : 300, height: 300, keepAspectRatio : false, saveToGallery : true};
                Camara.takePicture(options).
                    then((ImageAsset) => {
                        console.log("tamano: " + ImageAsset.options.width + "x" + ImageAsset.options.height);
                        console.log("keepAspectRatio: " + ImageAsset.options.keepAspectRatio);
                        console.log("Foto guaradada!");
                        SocialShare.shareImage(ImageAsset, "enviar imagen");
                    }).catch((err) => {
                        console.log("error -> " + err.message);
                    });
            },
            function failure() {
                console.log("Permiso de camara no aceptado por el usuario");
            }
        );
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
