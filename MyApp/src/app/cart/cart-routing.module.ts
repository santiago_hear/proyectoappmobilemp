import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { CartComponent } from "./cart.component";

const routes: Routes = [
    { path: "", component: CartComponent },
    { path: "item", loadChildren: () => import("~/app/item/item.module").then((m) => m.ItemModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CartRoutingModule { }
