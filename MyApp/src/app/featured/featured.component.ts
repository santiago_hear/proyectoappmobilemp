import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, GestureEventData, GridLayout } from "@nativescript/core";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(ev : GestureEventData)
    {
        console.log("Objeto que disparo el evento: " + ev.object);
        console.log("Vista que disparo el evento: " + ev.view);
        console.log("Nombre del evento: " + ev.eventName);

        const grid = <GridLayout> ev.object;
        grid.rotate = 0;
        grid.animate({
            rotate : 360,
            duration : 2000
        });
    }
}
