import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "@nativescript/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { NoticiasService } from "././domain/noticias.service";
import { CarritoService } from "./domain/carrito.service";
import { ProductosService } from "./domain/productos.service";
import { FavoritosService } from "./domain/favoritos.service";

import { initializeNoticiasState, NoticiasEffects, NoticiasState, reducersNoticias } from "./domain/noticias-state.model";

import {StoreModule as NgRxStoreModule } from "@ngrx/store"
import { ActionReducerMap } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { FavoritosState, initializeFavoritosState, reducersFavoritos } from "./domain/favoritos-state.model";

//redux init
// tslint:disable-next-line:interface-name

export interface AppState {
    noticias : NoticiasState;
    favoritos : FavoritosState;
}

const reducers : ActionReducerMap<AppState> = {
    noticias : reducersNoticias,
    favoritos : reducersFavoritos
};

const reducerInitialState =  {
    noticias : initializeNoticiasState(),
    favoritos : initializeFavoritosState()
};
// fin redux init

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, {initialState : reducerInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    providers : [NoticiasService, CarritoService, ProductosService, FavoritosService],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
