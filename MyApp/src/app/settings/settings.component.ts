import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as Toast from "nativescript-toasts";
import * as app from  "@nativescript/core/application";
import * as dialogs from "@nativescript/core/ui/dialogs";
import * as appSettings from '@nativescript/core/application-settings';
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    constructor(private routerExtensions : RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    doLater(fn) 
    {
        setTimeout(fn, 100);
    }

    ngOnInit(): void {
        /*
        this.doLater( () => 
            dialogs.action("Mensaje", "Cancelar!",["Opcion1", "Opcion2"])
            .then((result) => {
                console.log("resultado: " + result);
                if( result === "Opcion1") {
                    this.doLater(() => 
                        dialogs.alert({
                            title: "alerta 1",
                            message : "mensaje de alerta 1",
                            okButtonText : "btn 1"
                        }).then(() => console.log("Cerrado 1!")));
                }else if (result === "Opcion2") {
                    this.doLater(() => 
                        dialogs.alert({
                            title : "alerta 2",
                            message : "mensaje de alerta 2",
                            okButtonText : "btn 2"
                        }).then(() => console.log("Cerrado 2!")));
                }
            })
        );
        */
       const toastOptions : Toast.ToastOptions = { text : "Mensaje Toast", duration : Toast.DURATION.SHORT};
       this.doLater(() => Toast.show(toastOptions));
    }
    
    getUser() : string
    {
        if(!appSettings.hasKey("username"))
        {
            appSettings.setString("username","Default User");
        }
        return appSettings.getString("username");
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
