import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { SettingsComponent } from "./settings.component";
import { UserconfigComponent } from "./user-config/user-config.component";
//import { UserconfigComponent } from "./userconfig/userconfig.component";

const routes: Routes = [
    { path : "", component: SettingsComponent },
    { path : "userconfig", component : UserconfigComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SettingsRoutingModule { }
