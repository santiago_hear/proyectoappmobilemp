import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";

import { UserconfigComponent } from "./user-config.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule
    ],
    declarations: [
        UserconfigComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class UserconfigModule { }
