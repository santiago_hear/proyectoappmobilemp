import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as appSettings from '@nativescript/core/application-settings';
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Userconfig",
    templateUrl: "./user-config.component.html"
    // providers : [NoticiasService]
})
export class UserconfigComponent implements OnInit {

    textFieldValue : string = "";
    nombreActual : string = "nnn";

    constructor(private routerExtensions : RouterExtensions) {
        this.nombreActual = appSettings.getString("username");
    }

    ngOnInit(): void {
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(ruta : string) : void {
        console.log(this.textFieldValue);
        appSettings.setString("username", this.textFieldValue);

        this.routerExtensions.navigate([ruta], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }
}
