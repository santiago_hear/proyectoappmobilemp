import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { RouterExtensions } from "@nativescript/angular";
import { ProductosService } from "../domain/productos.service";
import { FavoritosService } from "../domain/favoritos.service";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { Favorito, LeerAhoraAction } from "../domain/favoritos-state.model";

@Component({
    selector: "Favoritos",
    templateUrl: "./favoritos.component.html"
})
export class FavoritosComponent implements OnInit {

    constructor(public favoritos : FavoritosService, private routerExtensions: RouterExtensions, private store : Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
    }

    leerAhora(args)
    {
        console.log(args.view.bindingContext);
        this.store.dispatch( new LeerAhoraAction( new Favorito (args.view.bindingContext)));
    }


    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }
}
