import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { FavoritosComponent } from "./favoritos.component";

const routes: Routes = [
    { path: "", component: FavoritosComponent },
    { path: "vista-detalle", loadChildren: () => import("~/app/vista-detalle/vista-detalle.module").then((m) => m.VistaDetalleModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class FavoritosRoutingModule { }
