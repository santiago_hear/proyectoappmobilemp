import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { RouterExtensions } from "@nativescript/angular";
import { ProductosService } from "../domain/productos.service";

@Component({
    selector: "Listado",
    templateUrl: "./listado.component.html"
})
export class ListadoComponent implements OnInit {

    constructor(public productos : ProductosService, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.productos.agregar("producto 1");
        this.productos.agregar("producto 2");
        this.productos.agregar("producto 3");
    }

    onPull(e)
    {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout( () => {
            this.productos.agregar("New producto x");
            pullRefresh.refreshing = false;
        },2000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }
}
