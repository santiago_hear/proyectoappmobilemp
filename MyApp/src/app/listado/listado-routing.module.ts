import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { ListadoComponent } from "./listado.component";

const routes: Routes = [
    { path: "", component: ListadoComponent },
    { path: "vista-detalle", loadChildren: () => import("~/app/vista-detalle/vista-detalle.module").then((m) => m.VistaDetalleModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ListadoRoutingModule { }
