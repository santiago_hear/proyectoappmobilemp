import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";


// ESTADO
export class Favorito {
    constructor(public texto : string) { }
}

// tslnt:disable-next-line:interface-name
export interface FavoritosState {
    items : Favorito [];
}

export function initializeFavoritosState () {
    return {
        items : []
    };
}

//ACCIONES
export enum FavoritosActionTypes {
    INIT_MY_DATA = "[Favoritos] Init My Data",
    LEER_AHORA = "[Favoritos] Nueva"
}

// tslnt:disable-next-line:max-classes-per-file
export class InitMyDataAction implements Action {
    type = FavoritosActionTypes.INIT_MY_DATA;
    constructor (public favoritos : Array<string>) { }
}

// tslnt:disable-next-line:max-classes-per-file
export class LeerAhoraAction implements Action {
    type = FavoritosActionTypes.LEER_AHORA;
    constructor (public favorito : Favorito) { }
}

export type FavoritosActions = LeerAhoraAction | InitMyDataAction;

// REDUCERS
export function reducersFavoritos(state : FavoritosState, action : FavoritosActions) : FavoritosState
{
    switch (action.type)  {
        case FavoritosActionTypes.INIT_MY_DATA : {
            const titulares : Array<string> = (action as InitMyDataAction).favoritos;

            return {
                ...state,
                items : titulares.map((t) => new Favorito(t))
            };
        }
        case FavoritosActionTypes.LEER_AHORA : {
            return {
                ...state,
                items : [...state.items, (action as LeerAhoraAction).favorito]
            };
        }     
    }

    return state;
}
