import { error } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { getJSON, request } from "@nativescript/core/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class ProductosService
{
    api : string = "";
    private productos : Array<string> = [];

    constructor()
    {
        // this.getDb((db) => {
        //     console.dir(db);
        //     db.each("select * from logs",
        //      (err, fila) => console.log("fila: ", fila),
        //      (err, totales) => console.log("Filas totales: ", totales));
        // }, () => console.log("error on getDB"));
    }

    getDb(fnOk, fnError)
    {
        return new sqlite("my_db_logs", (err, db) => {
            if(err) {
                console.error("Error en apertuda de DB", err);
            }
            else {
                console.log("Esta abierta la db", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXIST logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                .then((id) => {
                    console.log("Creacion correcta de la tabla");
                    fnOk(db);
                }, (error) => {
                    console.log("Error al crear la tabla", error);
                    fnError(error);
                });
            }
        });   
    }

    agregar(s : string) 
    {
        this.productos.push(s);
    }

    buscar()
    {
        return this.productos;
    }
}