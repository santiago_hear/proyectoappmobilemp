import { Injectable, OnInit } from '@angular/core';
import { getDb } from "../persistence/favoritosdb";

@Injectable()
export class FavoritosService
{
    favoritos : string[] = [];
    constructor()
    {
        getDb((db) => {
            console.dir(db);
            db.each("select texto from favoritos",
             (err, fila) => this.favoritos.push(fila[0]),
             (err, totales) => console.log("Filas totales fav: ", totales));
        }, () => console.log("error on getDB fav"));
    }

    agregarFavorito (s : string)
    {
        getDb((db) => {
            db.execSQL("insert into favoritos (texto) values (?)", [s],
            (err, id) => console.log("nuevo fav id: ", id));
        }, () => console.log("error en insert favs"));
    }

    getFavoritos() : string []
    {
        console.log("la lista: " + this.favoritos);
        return this.favoritos;
    }
}