import { Injectable } from '@angular/core';

@Injectable()
export class CarritoService
{
    private items : Array<string> = [];

    agregar(s : string) 
    {
        this.items.push(s);
    }

    buscar()
    {
        return this.items;
    }
}