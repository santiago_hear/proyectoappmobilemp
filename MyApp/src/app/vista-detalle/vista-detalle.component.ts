import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { CarritoService } from "../domain/carrito.service";
import * as dialogs from "@nativescript/core/ui/dialogs";
import * as Toast from "nativescript-toasts";

@Component({
    selector: "VistaDetalle",
    templateUrl: "./vista-detalle.component.html"
})
export class VistaDetalleComponent implements OnInit {

    constructor(public carrito : CarritoService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
    }

    doLater(fn) 
    {
        setTimeout(fn, 100);
    }

    agregarAlCarrito(prod : string)
    {
        this.doLater( () => 
            dialogs.action("Añadir al carrito", "Cancelar",["Añadir"])
            .then((result) => {
                console.log("resultado: " + result);
                if( result === "Añadir") {
                    this.carrito.agregar(prod);
                    const toastOptions : Toast.ToastOptions = { text : "Producto Agregado al Carrito", duration : Toast.DURATION.SHORT};
                    this.doLater(() => Toast.show(toastOptions));
                }
            })
        );
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
