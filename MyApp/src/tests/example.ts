// A sample Jasmine test
// describe("A suite", function() {
//   it("contains spec with an expectation", function() {
//     expect(true).toBe(true);
//   });
// });

var noticias_state_model = require("~/app/domain/noticias-state.model");

describe("reducerNoticias", function () {
    it("should reduce init data", function () {
        // setup
        var prevState = noticias_state_model.initializeNoticiasState();
        var action = new noticias_state_model.initMyDataAction(["noticia 1", "noticia 2"]);
        // action
        var newState = noticias_state_model.reducerNoticias(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].titulo).toEqual("noticia 1");
    });
    it("should reduce new item added", function () {
        // setup
        var prevState = noticias_state_model.initializeNoticiasState();
        var action = new noticias_state_model.nuevaNoticiaAction(new noticias_state_model.Noticia("noticia 3"));
        // action
        var newState = noticias_state_model.reducerNoticias(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].titulo).toEqual("noticia 3");
    });
    // it("should reduce new item added v2", function () {
    //     // setup
    //     var prevState = noticias_state_model.initializeNoticiasState();
    //     var action = new noticias_state_model.nuevaNoticiaAction(new noticias_state_model.Noticia("noticia 3"));
    //     // action
    //     var newState = noticias_state_model.reducerNoticias(prevState, action);
    //     // assertions
    //     expect(newState.items.length).toEqual(1);
    //     expect(newState.items[0].titulo).toEqual("noticia 3");
    // });
});
