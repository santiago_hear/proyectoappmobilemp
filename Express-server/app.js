
var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var noticias = [ "Nueva Actualización de Windows",
                "Colombia vence a venezuela",
                "Messi, el salvador de argentina",
                "En sudamerica, la eliminatoria mas dificil del mundo",
                "El covid, sigue creciendo en europe"];

app.get("/get", (req, res, next) => 
    res.json(noticias.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misFavoritos = [];
app.get("/favs", (req, res, next) => res.json(misFavoritos));
app.post("/favs", (req, res, next) => {
    console.log(req.body);
    misFavoritos.push(req.body.nuevo);
    res.json(misFavoritos);
})